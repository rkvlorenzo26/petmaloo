package petmaloo.petfeeder.com.petfeeder;

import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

public class MainActivity extends AppCompatActivity {

    private static final String authkey = "f8748e1738b3489ab63808f166f9cf10";

    TextView status;
    Button btnSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        status = (TextView) findViewById(R.id.status);
        btnSwitch = (Button) findViewById(R.id.btnSwitch);

        btnSwitch.setText("Feed your pet!");

        final Handler handler1 = new Handler();
        final Runnable updateTask1 = new Runnable() {
            @Override
            public void run() {
                getHardwareStatus();
                handler1.postDelayed(this,5000);
            }
        };
        handler1.postDelayed(updateTask1,1000);
    }

    public void onoff(View view){

        switchOn();

        final Handler handler = new Handler();
        final Runnable updateTask1 = new Runnable() {
            @Override
            public void run() {

                switchOff();
            }
        };
        handler.postDelayed(updateTask1,2000);
    }

    public void switchOn(){

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);

        String action = "update";
        String pin = "D0";
        String url ="http://blynk-cloud.com/"+authkey+"/"+action+"/"+pin+"?value=50";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //to-do
                        btnSwitch.setText("Feeding....");
                        btnSwitch.setEnabled(false);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //to-do
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void switchOff(){
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);

        String action = "update";
        String pin = "D0";
        String url ="http://blynk-cloud.com/"+authkey+"/"+action+"/"+pin+"?value=0";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //to-do
                        btnSwitch.setText("Feed your pet!");
                        btnSwitch.setEnabled(true);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //to-do
            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void getHardwareStatus(){
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);

        String action = "isHardwareConnected";
        String url ="http://blynk-cloud.com/"+authkey+"/"+action;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        int length = response.length();
                        String getStatus = ""+response.substring(0,length);

                        if(length == 4){
                            status.setText("Hardware Status: Online");
                        }else if(length == 5){
                            status.setText("Hardware Status: Offline");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Unable to connect to the server.")
                .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
            }
        });

        // Add the request to the RequestQueue.
         queue.add(stringRequest);
    }
}
